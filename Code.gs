var url="https://docs.google.com/spreadsheets/d/1WigLBhJH3Q1brKyt7gpkc7KjBTw83sMFONpfrvmmMAg/edit#gid=0";

function getScriptUrl() {
 var url = ScriptApp.getService().getUrl();
 console.log(url)
 return url;
}

function doGet(e) {
  var sh=SpreadsheetApp.openByUrl(url);
  var sheets = sh.getSheets();
  var i
  var myws
  var kanarr=[]
  var rubarr=[]

  for(i in sheets){
    myws=sh.getSheetByName(sheets[i].getName());
    
    var listKanji = myws.getRange(2,1,myws.getRange("A2").getDataRegion().getLastRow()-1 ,1).getValues();
    var listRuby = myws.getRange(2,2,myws.getRange("B2").getDataRegion().getLastRow()-1,1).getValues();
    
    kanarr.push(listKanji.map(function(r){return r[0];}))
    rubarr.push(listRuby.map(function(r){return r[0];}))
  }
  var coba =kanarr[3].length
  console.log(coba)
 
  if (!e.parameter.page) {
    var tmp = HtmlService.createTemplateFromFile('pattern11a')
    
     tmp.listKanji = kanarr;
     tmp.listRuby = rubarr;
     
     return tmp.evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME); 
     
  }else{
  
    var tmp =  HtmlService.createTemplateFromFile(e.parameter['page'])
     tmp.listKanji = kanarr;
     tmp.listRuby = rubarr;

  
    return tmp.evaluate().setSandboxMode(HtmlService.SandboxMode.IFRAME);
  }
}

function include(filename){
  return HtmlService.createHtmlOutputFromFile(filename)
  .getContent();
}
