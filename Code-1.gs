var url="https://docs.google.com/spreadsheets/d/1WigLBhJH3Q1brKyt7gpkc7KjBTw83sMFONpfrvmmMAg/edit#gid=0";

function getScriptUrl() {
 var url = ScriptApp.getService().getUrl();
 return url;
}

function doGert(e) {
  var sh=SpreadsheetApp.openByUrl(url);
  var ws = sh.getSheetByName("1課1");
  var ws2 = sh.getSheetByName("1課2");
  var ws3 = sh.getSheetByName("1課3");
 
 //スプレッドシートのコラムによってでスプレッドシートのデータを別々に格納します
  var listKanji = ws.getRange(2,1,ws.getRange("A2").getDataRegion().getLastRow()-1 ,1).getValues();
  var listHira = ws.getRange(2,2,ws.getRange("B2").getDataRegion().getLastRow()-1,1).getValues();
  
  var listKanji2 = ws.getRange(2,4,ws.getRange("D2").getDataRegion().getLastRow()-1 ,1).getValues();
  var listHira2 = ws.getRange(2,5,ws.getRange("E2").getDataRegion().getLastRow()-1,1).getValues();
  
  var listKanji3 = ws.getRange(2,7,ws.getRange("G2").getDataRegion().getLastRow()-1 ,1).getValues();
  var listHira3 = ws.getRange(2,8,ws.getRange("H2").getDataRegion().getLastRow()-1,1).getValues();
  
  var listKanji4 = ws2.getRange(13,2,ws2.getRange("B13").getDataRegion().getLastRow()-15,1).getValues();
  var listRb1 = ws2.getRange(13,5,ws2.getRange("E13").getDataRegion().getLastRow()-15,1).getValues();
  
  var listKanji5 = ws2.getRange(13,3,ws2.getRange("C13").getDataRegion().getLastRow()-12,1).getValues();
  var listRb2 = ws2.getRange(13,6,ws2.getRange("F13").getDataRegion().getLastRow()-12,1).getValues();
  
  var listKanji6 = ws3.getRange(16,2,ws3.getRange("B16").getDataRegion().getLastRow()-18,1).getValues();
  var listRb3 = ws3.getRange(16,6,ws3.getRange("F16").getDataRegion().getLastRow()-18,1).getValues();
  
  var listKanji7 = ws3.getRange(16,3,ws3.getRange("C16").getDataRegion().getLastRow()-15,1).getValues();
  var listRb4 = ws3.getRange(16,7,ws3.getRange("G16").getDataRegion().getLastRow()-15,1).getValues();
  
  var listKanji8 = ws3.getRange(16,4,ws3.getRange("D16").getDataRegion().getLastRow()-15,1).getValues();
  var listRb5 = ws3.getRange(16,8,ws3.getRange("H16").getDataRegion().getLastRow()-15,1).getValues();
  
 
  console.log(listRb4)
  
  if (!e.parameter.page) {
    var tmp = HtmlService.createTemplateFromFile('pattern11a')
    tmp.listKanji = listKanji.map(function(r){return r[0];});
    tmp.listHira = listHira.map(function(r){return r[0];});
    tmp.listKanji2 = listKanji2.map(function(r){return r[0];});
    tmp.listHira2 = listHira2.map(function(r){return r[0];});
    tmp.listKanji3 = listKanji3.map(function(r){return r[0];});
    tmp.listHira3 = listHira3.map(function(r){return r[0];});
    tmp.listKanji4 = listKanji4.map(function(r){return r[0];});
    tmp.listRb1 = listRb1.map(function(r){return r[0];});
    tmp.listKanji5 = listKanji5.map(function(r){return r[0];});
    tmp.listRb2 = listRb2.map(function(r){return r[0];});
    tmp.listKanji6 = listKanji6.map(function(r){return r[0];});
    tmp.listRb3 = listRb3.map(function(r){return r[0];});  
    tmp.listKanji7 = listKanji7.map(function(r){return r[0];});
    tmp.listRb4 = listRb4.map(function(r){return r[0];});
    
    tmp.listKanji8 = listKanji8.map(function(r){return r[0];});
    tmp.listRb5 = listRb5.map(function(r){return r[0];});
    return tmp.evaluate(); 
  }else{
    var tmp =  HtmlService.createTemplateFromFile(e.parameter['page'])
    tmp.listKanji = listKanji.map(function(r){return r[0];});
    tmp.listHira = listHira.map(function(r){return r[0];});
    tmp.listKanji2 = listKanji2.map(function(r){return r[0];});
    tmp.listHira2 = listHira2.map(function(r){return r[0];});
    tmp.listKanji3 = listKanji3.map(function(r){return r[0];});
    tmp.listHira3 = listHira3.map(function(r){return r[0];});
    tmp.listKanji4 = listKanji4.map(function(r){return r[0];});
    tmp.listRb1 = listRb1.map(function(r){return r[0];});
    tmp.listKanji5 = listKanji5.map(function(r){return r[0];});
    tmp.listRb2 = listRb2.map(function(r){return r[0];});
    tmp.listKanji6 = listKanji6.map(function(r){return r[0];});
    tmp.listRb3 = listRb3.map(function(r){return r[0];});   
    tmp.listKanji7 = listKanji7.map(function(r){return r[0];});
    tmp.listRb4 = listRb4.map(function(r){return r[0];});
    
    tmp.listKanji8 = listKanji8.map(function(r){return r[0];});
    tmp.listRb5 = listRb5.map(function(r){return r[0];});
    return tmp.evaluate(); 
  }
   
}



/*
function getSSData() {
  var ssId = '1WigLBhJH3Q1brKyt7gpkc7KjBTw83sMFONpfrvmmMAg';
  var ss = SpreadsheetApp.openById(ssId);
  var s = ss.getSheetByName('Sheet1');
  var lastRow = s.getLastRow();
  var lastCol = s.getLastColumn();
  var a_values;
  if(lastRow>0){
    a_values = s.getRange(1, 1, lastRow, lastCol).getValues();
  }
  console.log(a_values);
  return JSON.stringify(a_values);
}*/

function include(filename){
  return HtmlService.createHtmlOutputFromFile(filename)
  .getContent();
}